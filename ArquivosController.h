#pragma once
#include "Cliente.h"

class ArquivosController
{
private:
	string nomeArquivo;
public:
	ArquivosController();
	bool salvarClienteEmTxt(Cliente cliente);
	~ArquivosController();
	bool criacaoDeArquivo();
	void visualizarArquivo();
	void cadastrarCliente(Cliente cliente);
};

