#pragma once
#include <string>
#include <iostream>


using namespace std;
class Cliente
{
private:
	int id;
	string nome;
	float saldo;
public:
	Cliente();
	~Cliente();
	void setNome(string nome);
	string getNome();
	void setId(int id);
	int getId();
	void setSaldo(float saldo);
	float getSaldo();
	void teste();
};

